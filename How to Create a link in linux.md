
Always remember links not work when not linked with full paths

example: when you are in a directory of a file of which you want to create a link in another directory
in this case
Do not do this: ln -s filename /home/user/.local/dir/linkname
neither this ln -s ./filename /home/user/local/dir/linkname

Instead do this
ln -s /home/user/dir/filename /home/user/local/dir/linkname
always include full path of both source and link files
