	  # Some cool stuff

            - browser independent bookmarks with dmenu https://youtu.be/81OQtU8E0vE
	    - Dunst brodie https://youtu.be/-Ky9YgvUa40
	    - dragon drag and drop from terminal https://youtu.be/cbegEIczdNQ
	    - Glow pretty markdown rendering https://youtu.be/EDNpK3iH7A0
	    - xdo https://youtu.be/GkGVmuiOUXg
	    - promptless https://youtu.be/iy7WweiqGfw
	    - scrollback in st https://youtu.be/DXr5Hi9w6gA
	    - simple webcam https://youtu.be/SdY8EuI690Y
	    - virtual monitor with xrandr https://youtu.be/N9KxpPyJMJA
	    - v4l-utils webcam setting https://youtu.be/fPTSkTG1iE4
	    - distro flexing neofetch https://youtu.be/PZGt65p9g_s
	    - gotbletu terminal web browsing workflow https://youtu.be/z-vOQr8Ym-8
	    - Getting Started With Qutebrowser Distrotube https://youtu.be/_OJKp4c5OLs
	    - Luke Smith Qute Browser https://youtu.be/g2RtjO_jXvY
	    - Hex DSL Qute Browser https://youtu.be/NVATaZTQ7H8
