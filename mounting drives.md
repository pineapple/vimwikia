# Script for mounting drives
#!/bin/zsh

case "$1" in
	floo)
	     sudo mount /dev/sda3 /storage/threebroomsticks &&
	     sudo mount /dev/sda4 /storage/Ilvermony &&
             sudo mount /dev/sdb1 /storage/Hogsmead &&
             sudo mount /dev/sdb2 /storage/Muzix &&
	     sudo mount /dev/sdb3 /storage/Castelobruxo &&
	     sudo mount /dev/sdb4 /storage/Hogwarts &&
	     notify-send "You got Access in:
🍻 Three broomsticks
🌲 Ilvermony
🏘  Hogsmead
🎼 Muzix
🏯 Castelobruxo
🧙 Hogwarts"
	;;
	flew)
	     sudo umount /dev/sda3 &&
	     sudo umount /dev/sda4 &&
             sudo umount /dev/sdb1 &&
             sudo umount /dev/sdb2 &&
	     sudo umount /dev/sdb3 &&
	     sudo umount /dev/sdb4 &&
	     notify-send "(Enjoy Holidays) Regards-
🍻 Three broomsticks
🌲 Ilvermony
🏘  Hogsmed
🎼 Muzix
🏯 Castelobruxo
🧙 Hogwarts"
	;;
         *)
	   notify-send "Do you want to floo in or flew out from portals" ;;
esac
