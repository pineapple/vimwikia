# Playing with font preview
`fc-list | cut -d ":" -f 1 | dmenu -i -l 15 | xargs display {}`

`fc-list | cut -d ":" -f 1 | fzf --preview="display {-1}"
