*9 Types of Intelligence.*

IQ tests, for instance, are a common way to test intelligence
but most of these test examine proficiency in spacial, verbal, or mathematical intelligence American psychologist *Howard Gardner* thought this was strange.
As there are many ways people can be intelligent
He then developed his Theory of Multiple Intelligences,
which describes nine areas where people can be proficient

1. Naturalist Intelligence
Think of Katniss Everdeen from the Hunger Games, Chuck Noland from cast away and Julia from Julia of the Wolves.
With a keen eye to detail and easily noticing changes in their surroundings people with high Naturalist Intelligence will bevery good at surviving in the wild.

They have a cunning ability to distinguish between different animals, plants, clouds, rocks, etc. In today's modern world they are able to distinguish between different kinds of make up, cars, smart phones, and other items.

2. Musical Intelligence
Having a high Musical Intelligence indicates you are very good at things to do wit sounds. These can include characteristics such as rhythm, tone, and pitch.
If you've got a high musical intelligence, you're good at recognizing, creating, and reproducing music.
You might even have a high Mathematical Intelligence too. As these two things often involve similar thought processes.
If you happen to play an instrument and have a high Musical Intelligence, you're probably quite good at playing by ear.

3. Logical Mathematical Intelligence
People with a high score in Logical Mathematical Intelligence are good at abstract symbolic thought using letters in formulas to indicate relationships between factors.
and reasoning skills such as inductive and deductive reasoning
They are good at considering hypothesis, carrying out calculations, and quantifying data.

4. Existential Intelligence
People with high Existential Intelligence are good philosophical thinkers.
They're good at philosophizing and thinking about questions that might not have a real definite answer.
They have a sensitivity towards the deeper questions of life, and like challenging related concepts. They're not easily confused by their own feelings.

5. Interpersonal Intelligence
People with a high Interpersonal Intelligence are often at communicating with others. They are good at noticing shifts in other moods and motives as well as taking multiple perspectives on a subject. They often score high on empathy and do well in socially oriented work such as teaching, social work, and acting.

6. Bodily-kinesthetic Intelligence
People with this type of intelligence love movement. They have high body awareness and good moter skills, because of that they often excel in activities that involve body movement such as sports and dance. They learn best by doing and are keen to figuring things out through experimentation.

7. Linguistic Intelligence
People high in Linguistic Intelligence are better than average at understanding subtle differences in meaning.
They tend to pick up new words and languages quicker. It's a widely shared skill, often found in people that work in journalism, teaching, or even public speaking.
Furthermore, people with a high Linguistic Intelligence tend to have a broader vocabulary than average Children and young adults with a high Linguistic Intelligence tend to be drawn to language oriented activities. Such as, reading, writing, word puzzles, and telling stories.

8. Intra-personal Intelligence
People with a high Intra-personal Intelligence are good at understanding their own feelings and thoughts; and working them to achieve their goals.
They are good at motivating themselves from the inside-out without any visible or physical reward. It involves more then just understanding and planning their goals.
but also a less definite appreciation of the human condition.
People with a high Intra-personal Intelligence are often good philosophers and psychologists.

9. Spatial Intelligence
People with a high Spatial Intelligence are good at things that require mental manipulations of physical perspectives.
These people tend to be very skilled at map reading, jigsaw puzzles, and seeing or drawing objects and situations from a different perspective without necessarily having seen it from that perspective.
They do not easily get lost and are often at working their way around a maze. Spatial reasoning, image manipulation, and graphic, and artistic skills are often well developed in people with a high Spatial Intelligence.
People with this skill are often good at sailing, being a pilot, sculpting, painting, or designing architecturalfeatures in spaces.
Young people with good Spacial Intelligence areoften drawn to visually stimulating activities, like drawing.

Courtesy: [Psych2Go](https://www.youtube.com/watch?v=w7-rYp-BQJQ&feature=youtu.be)
#Psychology
