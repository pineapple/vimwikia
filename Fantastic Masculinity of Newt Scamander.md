
*The Fantastic Masculinity of Newt Scamander*

We, as movie-going audiences, have been conditioned to expect a certain type of masculine performance from male characters in sci-fi or fantasy films. We expect leading men who are, or learn to be, autonomous, brazen, and physically strong. Or at least men who are witty, boisterous, and charismatic. Preferably all of the above. It's practically required for male heroes to hide their vulnerability. We've learned to easily forgive aggression and arrogance in men but to take exception at presentations of humility or sensitivity. We're accustomed to seeing men who are quick to violence and slow to diplomacy. Newt is a significant departure from this trend. His version of manhood doesn't stem from physical strength or combat skills or feats of daring-do, or even from some preordained mystic destiny like so many other male heroes. He's sincere, nurturing, emotional, and sensitive. And critically that sensitivity is framed as a strength rather than a weakness.

Newt Scamander, protagonist of the Harry Potter spinoff, is an unconventional male hero. He performs a refreshingly atypical form of masculinity, especially for the lead in a fantasy adventure story.

#FantasticBeastAndWhereToFindThem
#NewtScamander
#TheMagizoologist

https://youtu.be/C4kuR1gyOeQ
