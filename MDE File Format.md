*MDE*

A file with the MDE file extension is a Compiled Access Add-in file used to store a Microsoft Access MDA file in binary format.

Advantages of MDE files include a compacted file size, VBA code that can run but can't be changed, and the ability to edit data and run reports while shielding the user from having full database access.

Other MDE files might be unrelated to MS Access and instead be add-in files used with the architectural design software ArchiCAD to extend the program's functionality

How to Open an MDE File

MDE files can be opened with Microsoft Access and probably some other database programs as well.

Tip: You can make an MDE file in Access through the Tools >> Database Utilities >> Make MDE File... menu option.

Microsoft Excel will import MDE files, but that data will then have to be saved in some other spreadsheet format like XLSX or CSV.

Graphisoft ArchiCAD's add-in files that have the .MDE file extension can be opened with that program.

Note: If you find that an application on your PC does try to open the MDE file but it's the wrong application or if you would rather have another installed program open MDE files, see our How to Change the Default Program for a Specific File Extension guide for making that change in Windows.

How to Convert an MDE File

Read the instructions on Granite Consultingand Pruittfamily.com for some information on converting an MDE file to MDB.

Once the information in the MDE file is in the MDB format, you can convert the MDB file to ACCDB or ACCDE using Microsoft Access.

A tool like MDE Compiler should be able to convert your MDE file to EXE to create a standalone program.

Still Can't Open Your File?

If the programs above aren't working to open your MDE file, it's possible that you're misreading the file extension and you don't actually have an MDE file. 

For example, an Amiga MED Sound file and RSView Development Project file both use the MED file extension which is really similar to MDE but not the same.


Even though they look like they might be related to Microsoft Access or ArchiCAD, they instead open with ModPlug Player and RSView, respectively. 

The same is true for other file extensions that might sound or look like "MDE," like MME which belongs to the Multi-Purpose Internet Mail format, or MDD which might be a Point Oven Deformation Data file or an MDict Resource file.
