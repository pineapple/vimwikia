NOTES FROM LUKE'S VIDEO
https://youtu.be/-dEuXTMzRKs

# Simply Installing a program use -S
`sudo pacman -S emacs`
# To Updating database
`sudo pacman -Syu`
-S		=	sync package to remote repositories
-Sy		=	Just sync
-u		=	update a program

# `sudo pacman -Syyu` double checks sync and install updates
# 'w' flags downloads in the background update and not install it.
	`sudo pacman -Syyuw`

# In debian based distros you get these
	apt-get update && apt-get upgrade
# which is equivalent to these in arch
	pacman -Sy && pacman -Su
# but you can run this as one like this
	pacman -Syu

# To search for programs in remote repositories
	`pacman -Ss emacs`
# To search for programs in local repositories
	`pacman -Qs emacs`

# Uninstalling packages
	`pacman -R vidir`	- 	Just remove the program
	`pacman -Rs vidir`	- 	To remove with dependancie
	`pacaman -Rn vidir`	- 	To remove with system config (not config (dotfiles))
	# So the best way to uninstall is using
		`pacman -Rns vidir`
	# To remove package with all dependant packages [be cautious here]
		`pacman -Rsc vidir`
	# but if need to delete without removing dependant packages
		`pacman -Rdd vidir`

# Listing out programs
	`pacman -Q`	-	List out all packages installed in system
	`pacman -Qe`	-	List out user explicitely installed packages
	`pacman -Qn`	- 	List out programs installed from main repositories
	`pacman -Qm`	-	List out programs installed from AUR

# Extra Utilities
	`pacman -Qdt`	-	No longer needed programs (orphaned packages)

# Configuring /etc/pacman.conf
# uncomment any flags you want to use by removing # hashtags
`Color`			- Enable coloring
`CheckSpace`		- Enable checkspacing
`ILoveCandy`		- Enable pacman ascii animation
`VerbosePackageList`	- Enable detailed information about package changes
`IgnorePkg = libseccomp`  - To stop a program from being upgraded
# `sudo pacman -Syu --ignore=vlc` - To ignore temporarily
`HoldPkg = Glibc`	- To hold a package to install

# To downgrad any packages
	`pacman -U /var/cache/pacman/pkg/package-old_version.pkg.tar.type`
# `-U` is for installing from local file

# You can change mirrorlist from here
	`/etc/pacman.d/mirrorlist`

# Cleaning Up
When you update packages to newer version older version of packages are still there.
SO if you want to clear out all these older packages from caches do this.
	`df -h` 	- to check disk size
	`pacman -Sc`	- To clear caches

WARNING: You upcan remove pacman caches with this command `sudo pacman -Sc` but
it also clear the caches of your installed packages which are needed when
package update doesnt work and you have to downgrade until the issue is fixed.
Thats not a big disaster because you can easily downgrade a package using Arch repo.
But be careful before you use this. And DO NOT use this oftenly. ONLY once in a year or so.
Source: https://endeavouros.com/docs/pacman/paccache/


# Using yay
       installing yay - git clone https://aur.archlinux.org/yay.git ; cd yay ; makepkg -si

       yay -S package	- Install package
       yay -Rns Package	- Remove package
       yay <Search Term>	- Searching yay
       yay -Ss package	- search package
       yay -Syu 	- upgrade all install packages (similar to pacman -Syu)
       yay -Yc	- To clean unneeded dependancies
       yay -Ps	- Prints system statistic
       yay -Sw package	- To download package without installing it
       yay -Si package	- get information of package
       yay -Sua	- To update all installed aur packages
       yay -u package	- To update single package
