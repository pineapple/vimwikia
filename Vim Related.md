

o - opens the new line below cursor
O - opens the new line above cursor

:w <filename> - saves as to filename
:!<cmd> - exectute external command
v :w <filename> - saves the current cursor line in filename document
:r <filename> - retrieve the text from filename document and insert into into the document at cursor

x - delete the word
r - replace the word
ce - delete or change the word

(c-G) - to see the status
line number and G - to goto the line number

gg - goto to top of document
GG - got to the bottom of document

/word - search the word
?word - search the word

/s/oldword/newword - substitute oldword with newword in line at cursor

/s/x/y/g - substitute x with y globally

/s/x/y/gc - substitute x with y globally but asking confirmation at every instance of occurance

:%s/old/new/g - substitute old with new in the whole file
:%s/old/new/gc - substitute old with new in the whole file with confirmation prompt

:s/thee/the - simple substitute command
:s/thee/the/g - simple substitute command for all occurrences in line

% to spawn cursor at other side of the brackets

lesson 6.2
