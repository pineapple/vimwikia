Use Mouse with Keyboard

 To add mouse functionality to numpad keys
 add this line to `~/.xprofile` or `remaps` script from `~/.local/bin/`
 `setxkbmap -option keypad:pointerkeys &`

		 and

 this line to `/etc/X11/xorg.conf.d/00-keyboard.conf`
 add `Option "XkbOptions" "keypad:pointerkeys"`

		 and

 press `Ctrl+Shift+Numlock` to activate it.
