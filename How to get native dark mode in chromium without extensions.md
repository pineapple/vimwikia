put this link in urlbar of chromium based browsers
`brave://flags/#enable-force-dark`

and enable `Force Dark Mode for Web Contents` options
