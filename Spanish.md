============	Spanish Language Notes	=========================

Spanish called Espansol in spanish

pronounciations
El abecedario	=	The Alphabets

a = ah
b = beh
c = seh
d = deh
e = eh
f = efe
g = heh (breathing sound)
h = ahcheh
i = ee
l = ele
ll = eh-jeh
m = ehmeh
n = ehneh
N = ehnyeh
O = oh
p = peh
q = qu
r = ehreh (flap)
Rr = dobleh ehreh
s = ehseh
t = teh
o = ooh
v = oo-veh
w = (oo) dobleh-veh
x = equis
y = y-griega
z = zeta

geography: geografia = heografia

hola 			-	hello
¿Cómo te llamas?	- 	what is your name?
Me llamo + (your name).	-	My name is ...
¡Mucho gusto!		-	Nice to meet you!
¿Cómo estás?		-	How are you?
Estoy bien.		-	I am doing well.

Estoy mal.		-	I am not doing well.
¿Y tú?			-	And you?
gracias			-	thank you
adiós			-	goodbye
¡Hasta luego!		-	See you later!

`¡Hola! Me llamo Carlos. ¿Cómo te llamas?`
hello my name is Carlos, what is your name?

Como estas?
how are you
estoy bien
i am well
estoy mal
i am not doing well

ser: Conjugations of ser

Pronoun | Conjugations | Translation
-----------------------------------------
yo	| soy	       | I am
tu	| eres	       | you are
el,ella,usted | es	| he, she is / you (formal) are
nosotros | somos | we are
ellos, ellas, ustedes | son | they are, you are


yo soy rubia 	- I am blonde
tu eres doctor 	- you are a doctor
El es de panama	- he is from panama
Maria es casada y es abogada	- maria is married and is a laywer
Nosotros sonos americanos	-	we are americans
ellas son abogada	-	they are lawyers

To talk about where you or someone else is from, just follow this formula:
ser conjugation + de + place

Yo soy de México.
I am from Mexico.

Celia es de España.
Celia is from Spain.

Elizabeth es de los Estados Unidos.
Elizabeth is from the United States.

Felipe es de Bogotá.
Felipe is from Bogotá.

Ella es de Madrid.
She is from Madrid.

¿De dónde eres?
Where are you from?

Yo soy de México.
I'm from Mexico.


quero = I want

¿Qué quiere usted comer?

primero, yo quiero una ensalada

buenas noches = good evening

comer = to eat
de nada = your welcome
algo mas? = anything else
por favor = please
