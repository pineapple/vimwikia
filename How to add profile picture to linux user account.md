See the contents of ...

/var/lib/AccountsService/
/home/$USER/.face

    the 1st will have a desktop styled file
        the 2nd holds the image taken from a webcam in case you used that

	To change the 1st check the desktop file for the dir at Icon= and replace the file it points to (make sure yours is the same size). The second just requires you to move a file to it (probably in the same size and format).

	If you want to remove the picture altogether and default back to no profile picture, remove the property that says Icon=, or if you want one single command to do it for you:

	sed -i '/Icon/d' "/var/lib/AccountsService/$USER"
