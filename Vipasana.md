
Vi pashya na

vishesh prakar se dekhna

in pali = vipasana

**ye pranayam vidya ka dhyan swarup hai**

**samyak drishti ke abhyas se prapt hone wala samyak gyan**
arthta jaisa hai use waisa hi dekho aur waisa hi samjho
aur uske anusar hi acharan karo

bharat me yog tin prakar me bata hua tha

1. hathyog = pranayam wadi yog padhhati (yam, niyam, asan, pranayam (hathsamadhi prapt hoti thi (jadsamadhi)))
2. rajyog = ashtang kram yog (all asht ang yog ke)
3. bhaktiyog = navdha bhakti (shravan, kirtan, padseva upto aatmanivedan)

**mahatma budhha ne ise punarjivit kara**
pahle ki chiz ko fir se ujagar krna

ye prachin vidya hai sabne apne apne nam se ise upyog me liya

mahatma budhha
		- rajkumar raha hai
		- padha likha tha
		- us kal ke gyani se milkar satsang kra honga
		- aur purv janam ka to tha hi
		- aur sikhne aur abhyas krne ke uprant usko samaz me aya
		- raj tha isliye raj dharam hone ke wajah se inka sampraday jyada faila
		- yogi hone ke wajah se sabhi anuyayi bane (yudhha krne ki jarurat na thi)
		- prano ke bad man ko jita
