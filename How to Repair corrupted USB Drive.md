# IN WINDOWS WE USE INBUILT DISKPART TOOL

- Open Command Prompt as Administrator
- Type `diskpart`
- list the disks `list disk`
- select your usb drive `select disk 1`
- clean the filesystem on disk `clean`
- create primary partitinon `create partition primary`
- make it active `active`
- select it `select partition 1`
- format it `format fs fat32`
- wait to complete formating
- Done! Now you can exit `exit`
