*How I set up my Ncmpcpp*
[This setup is based on Luke Smith's voidrice config but i made some changes to make it work for me]

Step 1 : Install

`sudo pacman -S ncmpcpp`
or
`yay -s ncmpcpp`

Step 2 : Configure

- create config files:
  `nvim ~/.config/ncmpcpp/config`

- paste the content from here:
*[[ncmpcpp/config]]
*[[ncmpcpp/bindings]]

- Go to `~/.config/mpd` folder
  - create/edit config file
  - paste the content from here:
  *[[mpd/mpd.conf]]
    - Edit them according to your need like changing music directory and all.

- Inside `config/mpd` folder create these files and folder too
  - `mkdir playlists`
  - `touch mpd.conf mpd.db mpd.log mpd.pid mpdstate`

Step 3 : Initialize mpd and start ncmpcpp

- open terminal and follow instruction

  - kill mpd process
    `killall mpd`

  - start mpd
    `mpd`

  - start ncmpcpp
    `ncmpcpp`


Enjoy!
