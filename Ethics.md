	-------------------------------------  Ethics  --------------------------------

	Topics tries to cover {
		Ethics      --->   nitishastra
		Integrity   --->   satyanishta
		Aptitude    --->   abhiruchi
		} but famous only with one name = Ethics

	Approach {
		   -- Value Neutrality (mulya tatasthata) ---->
		   -- Reasoning mind (tarkik mastishq)	---->
		   -- Balancing Mind (Santulan mansikta) ----> Balancing mind errespective of place and era
		   -- Continum Approach (Satatya Mula Drishtikon) ----> exist in a continum scale but neither at extreme/ultimate  [A --|-|-|-|--|-|--|-|-- B]
		   -- Dichotomy Approach (Dwaidh Manowritti) ---> Either but not both

	Theory { Topics:
		  1. Introduction
		  2. Attitude (Abhiwritti)
		  3. Aptitude + Values (Abhiruchi + mulya)
		  4. Emotional Intelligence (Bhavnatmak Budhhimatta)
		  5. Philosophy of Ethics (Nitishastra Darshan ) ----> Bharat aur vishwa ke naitik chintko aur darshiko ka yogdan
		  6. Administrative Ethics (Prashasanik Naitikta)
		  7. Corruption (Bhrashtata)
		  }

	Theory From:
	1. Psychology::Social Psych:[2,3,4]
	2. Philosophy:[5]
	3. Public Admin:[6,7,3]
	4. Sociology:[1(&)]

	Case Studies(8):[6,7,3,,,2,4]

	1. Attitude:: ❌[yuva bhasha::badtamiji], ✅[ruzan]:: kisi ke prati sakaratmak ya nakaratmak ruzan
	2. Aptitude:: Pratibha
			 ├─ yogyata
			 ├─ janmajat (samanyatah)
	                 └─ Prashikshan ---> parinam
	3. Value:: mulya
		    ├─ naitik adarsh
		    ├─ sahishnuta
		    ├─ punctuality
		    ├─ imandari
		    └─ aatmasanman

	4. Emotional Intelligence
	   	│ 	└─ Situation ko samzne aur sambhalne ki buddhimatta
		└─ Kudh ke aur dusro ke emotions ko samazne aur sambhalne ki buddhimatta

	5. Conscience (antaraatma):: superego v/s id

	Topic 1 : Questions:
	             ⭐⭐⭐ 1. Naitikta kya hai? yah yeh wastunisht hoti hai ya aatma nishta?
		     ⭐⭐⭐⭐ 2. Naitik mulya kya hai? kuch wicharak mante hai ki n.m. nitya aur sthayi hote hai. jab ki kuch mante hai ki wo samay aur sthan ke anusar badlte rahte hai. is sambandh mai aapki ray kya hai spasth kre.
		     ⭐ 3. kya manushyake sabhi kritya naitik ya anaikit hi hote hai? kya aisi sambhawna hai ki koi kritya na naitik ho aur na anaitik. vichar kre.
		     ⭐🌜 4. naitik vyawastha kisi samaj ya prashasan ke liye kyu awashyak hai. samaj ko isse kya labh hote hai aur kya nuksan?
		     5. nimmalikhit ke bich samantaye, antar wa sambandh bataye. ---> 3⭐ 1. ethics tatha morality 2⭐2. ethics and religion 3⭐3. naitika aur kanun ⭐4. pap, bhul tatha apradh ⭐5. swabhav aur charitra
		     ⭐⭐6. kisi samaj me naitik mulyo ka nirdharan kin karko ke adhar par hota hai? kin karko ke adhar par samajik naitiktaye pariwartit hoti hai?
		     ⭐⭐7. kisi vyakti dwara kiya gaya koi kritya naitik hai ki nahi iska nirnay kin adharo par kiya ja sakta hai spasht kre.
		     ⭐⭐⭐8. kisi vyakri ke bhitar naitik mulya nirmit karne me nimmalikhit sansthao ki kya bhumika hoti hai.
		        a) pariwar b) samaj c) shiksha sansthaye
		     ⭐9. aisa kyu hota hai ki ek hi samaj ke vibhinna vyakti yo me na keval naitik mulya bhinna bhinna paye jate hai apitu mulyo ke prati unki pratibaddhata ka star bhi alag alag hota hai. spasth kijiye
		     ⭐⭐⭐10. kis samaj sudharak/prashasak/neta ko aap naitik rup se aadarsh mante hai. uske shikshayonka saar batate hute samzaye ki aap unhe apne jivan me kaha tak utar paye.
		     ⭐11. naitik pragati se aap kya samazte hai. kya aapko lagta hai. ke manav sabhyata ne paryapt naitik pragati ki hai?
		     ⭐⭐12. niji sambandho aur sarvajanik sambandho me naitikta kis prakar alag hoti hai? kya ye dono naitiktaye purnatah prithak hai ya paraspar sambandhit, udaharan dekar samzaye.


	 Philosophy
	     ├─ gyanmimansa (epistemology)
	     │ 
	     ├─ tatvamimansa (metaphysics)
	     │    └── prashna
	     │         ├─ ishwar (theology)
	     │         ├─ aatma
	     │         └─ jagat (sansar)
	     └─ nitishastra (ethics)
	           ├─ jivan ka Uddesh
		   │            ├─ moksha ----> 6 darshan
		   │            └─ sansar  ----> charvak darshan
		   └─ uske prapti ke sadhan
		          ├─ yog marg --> yam niyam asan pranayam pratyahar dharna dhyan samadhi
			  ├─ gyan marg --> shravan manan nididhyasan
			  └─ bhakti marg --> nam jap, devotion

* So philosophy wala ethics means jivan ka uddeshya kya hai aur usko prapt krne ke sadhan kya hai in sawalo ka uttar pana

ethics ka do matlab ho sakta hai
├─ philosophy wala -- darshanik*
└─ samaj shastra (sociology) ---> naitik vayawastha (ethical order moral order)

samaj (samaj khud ek vyavastha hai)
 ├─ samajik niyantran
 │      ├─ smajikaran (socialization)
 │      │     ├─ mother
 │      │     ├─ family
 │      │     ├─ neighborhood
 │      │     ├─ school/friends
 │      │     └─ film/tv/market
 │      └─ sactions
 │            ├─ purskar
 │            └─ dand
