#  ____  _____ ____ ___ ____  _____ ____
# |  _ \| ____/ ___|_ _|  _ \| ____/ ___|
# | | | |  _| \___ \| || |_) |  _| \___ \
# | |_| | |___ ___) | ||  _ <| |___ ___) |
# |____/|_____|____/___|_| \_\_____|____/

#==================================================================================
#==================================================================================


	⚜️ [Daily Goals](Ideal Schedule)
		- Python
		- emacs
   	- [Life Goals](Life Goals)
   		- Big Dreams
		- Little Impulses
	===========================================================================
   	- [Quick Thoughts](Quick Thoughts)
		- Way to make gtk apply xresources theme.
		- compiz in dwm
		+ shant urja
    	- To remember
      		- Making Twilight movie hindi audio with ffmpeg
    		- Making all keybinds maps in keyboard image
	===========================================================================
   	# Desires
	  	+ Read Japanese Superstitions books
 		+ reading that indrajal book
		+ shell prompt make on github
		+ appimage app integration
		+ marathi learning
 		+ asking if ~ can be replaced with other thing
 		+ asking for PEMDAS PE(M&D)(A&S)
		+ dynamic cursor patch to st
		+ try preview patch of pystardust
	===========================================================================
        # Work buffer
     		+ DIY clipboard
		+ fzf fm
