== [[Linux Related]] ==
[[Installation of Artix Linux]]
[[Important Regex Stuff]]
[Emacs Notes](Emacs Notes)

[[Everything Need to Know about Pacman]]
[[Everything Need to Know about FFMPEG]]
[[Everything Need to Know about Tmux]]
[[How to get fancy tty artwork in tty login]]

== [[FontsToDownload]] ==

== [[Some Challenges]] ==
== [[Vim Related]] ==
== [[Bash Scripting]] ==

== Installatation and Fixing ==
-[[How to setup ncmpcpp]]
-[[Installing dwm]]
-[[Pass - A standard Unix Password Manager]]
-[[Paccache - For unbloating your root Storage]]
-[[How to set figlet like artwork in tty console]]
-[[USB RELATED]]
-[[How to Ignore a package from being upgraded in Arch linux]]
-[[Patches How to work with them]]
-[[How to Downgrade Package]]
-[[init systems and services]]
-[[How to take Screenshot using ImageMagick]]

== Programming Related ==
-[[Bash Scripting Crash Course Beginners]]
-[[Bash Scripting Edureka Course]]
-[[Handling Git]]
-[[WebDesigning]]
-[[python notes]]

== How Tos and Tips and Tricks ==
-[[How to download gdrive links from websites all at once]]
-[[How to make Linux Terminal Banner]]
-[[How to remove viruses]]
-[[How to Repair corrupted USB Drive]]
-[[How to RIP games with Batch Script]]
-[[How to Transfer unfinished IDM downloads to Other PC]]
-[[How to install Fonts]]
-[[How to Install Grub theme]]
-[[How to use keyboard to use Mouse Pointer]]
-[[How to Create a link in linux]]
-[[How to get native dark mode in chromium without extensions]]
-[[How to add profile picture to linux user account]]

== Windows Related ==
-[[100+ Run Command List]]
-[[A-Z Index of Windows CMD command line]]
-[Windows Net Hunger Cure](Windows Net Hunger Cure)

== Knowledge ==
-[[MDE File Format]]
-[[Microprocessors 8080]]
-[[All sigkill commands]]

== Just Archieved Notes ==
-[[20 Secret Apps for Android]]
-[[Make you Internet faster and private with 1.1.1.1.]]
-[[Linux Apps must installed]]
-[[Must have Softwares for PC]]
-[[Virus Writing Tutorial]]

== Shortcuts to Search ==

"wa": "https://wiki.archlinux.org/?search={}"
"lib": "http://gen.lib.rus.ec/search.php?req={}"
"yt": "https://youtube.com/results?=search_query={}"

[[Rust programs to replace gnu core utilities]]

[[Important Testing]]

[[LearningPythonAgain](LearningPythonAgain)]
