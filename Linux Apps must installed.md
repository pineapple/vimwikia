
*Linux apps must installed*

🖊️ Marker: https://fabiocolacio.github.io/Marker
🎙️ Hammond: https://gitlab.gnome.org/alatiera/Ham...
 🖥️ HydraPaper: https://github.com/gabmus/hydrapaper
🔐 KeePassXC: https://keepassxc.org
💬 Matrix: https://matrix.org
 💬 Riot: https://riot.im
💬 Fractal: https://gitlab.gnome.org/danigm/fractal

Tomboy https://wiki.gnome.org/Apps/Tomboy
MyNoteX https://sites.google.com/site/mynotex/
Tojita http://trojita.flaska.net/
Kontact https://userbase.kde.org/Kontact
Osmo http://clayo.org/osmo/
Catfish http://www.twotoasts.de/index.php/cat...
KOrganizer https://userbase.kde.org/KOrganizer
Evolution https://help.gnome.org/users/evolutio...
Freeplane https://www.freeplane.org/wiki/index....
Calligra Flow https://www.calligra.org/flow/
