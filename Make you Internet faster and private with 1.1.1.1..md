
Make your Internet Faster with this little Trick! (This IS NOT A JOKE I Am Serious!)

Cloudflare, a well-known Internet performance and security company, announced the launch of 1.1.1.1—world’s fastest and privacy-focused secure DNS service that not only speeds up your internet connection but also makes it harder for ISPs to track your web history.

Domain Name System (DNS) resolver, or recursive DNS server, is an essential part of the internet that matches up human-readable web addresses with their actual location on the internet, called IP addresses.

For example, when you try to open a website, say wikipedia.com, your DNS looks up for the IP address linked to this domain name and load the site.

Since the default DNS services provided by ISPs are often slow and insecure, most people rely on alternative DNS providers—such as OpenDNS (208.67.222.222), Comodo DNS (8.26.56.26) and Google (8.8.8.8), to speed up their Internet.

But if you use Cloudflare new 1.1.1.1 DNS service, your computer/smartphone/tablet will start resolving domain names within a blazing-fast speed of 14.8 milliseconds—that’s over 28% faster than others, like OpenDNS (20.6ms) and Google (34.7ms).

Even if you are visiting websites over HTTPS, DNS resolvers log every site you visit, making your ISP or 3rd-party DNS services know about everything you do on the Internet.

“That means, by default, your ISP, every wifi network you’ve connected to, and your mobile network provider have a list of every site you’ve visited while using them,” the company says.

However, Cloudflare has changed this game with its new free DNS service, which it claims, will be “the Internet’s fastest, privacy-first consumer DNS service,” promising to prevent ISPs from easily tracking your web browsing history.

Cloudflare public DNS resolvers, 1.1.1.1 and 1.0.0.1 (as alternate DNS server for redundancy), support both DNS-over-TLS and DNS-over-HTTPS to ensure maximum privacy.

The company has also promised not to sell users’ data, instead to wipe all logs of DNS queries within 24 hours. It’s also working with auditors at KPMG to examine its systems and guarantee it’s not actually collecting your data.

Visit https://1.1.1.1/ from any device to get started with the Internet's fastest, privacy-first DNS service.

TUTORIAL How to:
Here is how you can set up 1.1.1.1 DNS for your devices, just follow this link: http://www.ethicalhackx.com/make-internet-faster-1-1-1-1-privacy-dns-server/

Also watch https://youtu.be/kqnvrjgyEMc for video explanation.
