*Fab 4 Math: Computer Maps Beatles' Musical Evolution*

Serious Beatles fans may be able to describe the band's complex musical evolution during its eight-year run, but now there is a mathematical way to map the group's progression from "Love Me Do" all the way to "Let It Be."

A group of researchers developed an algorithm that sorts out similarities among songs based on sound frequencies and patterns. The scientists then used the algorithm to analyze songs from each of the 13 Beatles albums released in the United Kingdom. After determining how closely related each song was, the algorithm successfully ranked the albums chronologically.

"People who are not Beatles fans normally can't tell that 'Help!' was recorded before 'Rubber Soul,' but the algorithm can," study author Lior Shamir, a professor at the Lawrence Technological University in Southfield, Michigan, said in a statement. "This experiment demonstrates that artificial intelligence can identify the changes and progression in musical styles by 'listening' to popular music albums in a completely new way." [Images: The World's Most Beautiful Equations]

The algorithm, which is described in the August issue of the journal Pattern Recognition Letters, converts each song into a visual map called a spectrogram. This diagram displays the changes in sound-wave frequency, shape and texture throughout the song. The algorithm then sorts and compares how closely the spectra of sound waves line up in each song. Lastly, a statistical analysis ranks how closely related two songs are to each other.

The algorithm determined that songs on the Beatles' first album, "Please, Please Me,"were most like the songs on the group's next recorded album, "With the Beatles." The early tunes were least similar to the songs on the band's last album, "Abbey Road."(Even though "Let It Be"was the last album the band released, the songs on the album were actually recorded before those on "Abbey Road," meaning the algorithm correctly identified the chronological order of the songs, despite the release dates.)

Shamir and his graduate student Joe George didn't stop at the Beatles: They also used the algorithm to analyze other well-known groups, such as U2, Tears for Fears and Queen. The algorithm spotted the similarities between two consecutive Tears for Fears albums, even though they were released 15 years apart: The band recorded "Seeds of Love"in 1989 right before breaking up, and "Everybody Loves a Happy Ending" was the first album released after the band reunited in 2004. The algorithm also correctly sorted Queen's discography and could distinguish between the albums recorded before and after "Hot Space" — the record that represented the most radical shift in the group's music.

Shamir and George hope the algorithm can be used to organize music databases and help users easily navigate and search through songs, artists and albums. For music streaming services like Spotify and Pandora that play music based on songs that users have "liked," the algorithm could be adapted to go one step further and identify music that matches a person's individual music preferences.

"A system can learn the musical preferences of a user by 'listening' to the music he or she listens to, and then constantly search[es] for more music he or she will probably also like, but might not be aware of," Shamir told Live Science in an email. "The information revolution allows every musician to make their creative work accessible to the public, but the main problem is discovering it in the vast flow of data."

Source: https://www.livescience.com/47061-musical-progression-algorithm-analyzes-beatles.html
