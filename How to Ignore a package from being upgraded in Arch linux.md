**Ignore A Package From Being Upgraded In Arch Linux**

To ignore a package from being upgraded, you need to add it to IgnorePkg or members of IgnoreGroup in the pacman.conf file.

Say for example, to ignore vlc from being upgraded while updating your Arch Linux you need to do the following.

Edit pacman.conf file:

`$ sudo vi /etc/pacman.conf`
Add the line – “IgnorePkg = vlc” as shown below.
```
# Pacman won't upgrade packages listed in IgnorePkg and members of IgnoreGroup
 #IgnorePkg =
 IgnorePkg = vlc
 #IgnoreGroup =
Save and close the file. To ignore a group of packages, you need to add IgnoreGroup = plasma-desktop, for example.
```
Whenever you try to update vlc, you will see a warning message like below.

`$ sudo pacman -S vlc`
Sample output:
```
:: vlc is in IgnorePkg/IgnoreGroup. Install anyway? [Y/n]
To remove this, simply remove or comment out the line IgnorePkg = vlc by adding a # sign in-front of it.

Now, again run the following command to update Vlc:

$ sudo pacman -S vlc
Sample output

resolving dependencies...
looking for conflicting packages...

Packages (1) vlc-2.2.4

Total Installed Size: 10.7 MiB
Net Upgrade Size: 2.15 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring [######################] 100%
(1/1) checking package integrity [######################] 100%
(1/1) loading package files [######################] 100%
(1/1) checking for file conflicts [######################] 100%
(1/1) checking available disk space [######################] 100%
:: Processing package changes...
(1/1) upgrading vlc [######################] 100%
```

As you see in the above output, Vlc has been upgraded to the latest version.

The above method will permanently stop the specified package from being upgraded until you remove it from the ignore list.

To do this temporarily, you can simply run:

`$ sudo pacman -Syu --ignore=vlc`
This command will skip vlc package from being updated. Similarly, to skip a package group, just run:

`$ sudo pacman -Syu --ignoregroup=plasma-desktop`
