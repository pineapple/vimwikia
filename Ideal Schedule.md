#=== Practices ===
 - Study spanish
 - practice dvorak
 - learn python
 - learn Emacs

# At 4.30 - 6.00
- Practice thoughtlessness  15m
- Tatva Meditation 15m
- Chakra Meditation 15 m
- Surya Namaskar 5m
- Cardiyo 10m
- Breathing Exercise 10m

# Things I wanted to learn
*Carreer Related*
- CEH - Certified Ethical Hacker
- Linux - Certified Linux Administrator
- HTB Academy - Hack the box
- Learn Webdesigning
- learn bash scripting
- Python
- Rust
- C/C++
- Java/JavaScript
- Machine Learning / Data Science
- Accounting
- Laws and Banking

*Non Career related*
- Reading Vedas
- Learn Japanese
- learn dvorak
- learn philosophy
- learn psychology
- learn linguistics
- learn spiritulogy
- learn IAS Syllabus
- Dancing
- Kungfu
- Under water swiming
- Magick
