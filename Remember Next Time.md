
[ missing firmware after upgrade ]

	When Syu says
    	==> WARNING: Possibly missing firmware for module: wd719x
    	==> WARNING: Possibly missing firmware for module: aic94xx

  	then
    	yay wd719x-firmware aic94xx-firmware


 [Zathura clipboard support]

	 To add clipboard support in zathura
	 `set selection-clipboard clipboard`
	 add this line to zathurarc

 [ Mouse in Numpad]

 	To add mouse functionality to numpad keys
	 `setxkbmap -option keypad:pointerkeys &`
 	add this line to `~/.xprofile` or `remaps` script from `~/.local/bin/`

		 and

	 add `Option "XkbOptions" "keypad:pointerkeys"`
	 this line to `/etc/X11/xorg.conf.d/00-keyboard.conf`
	 and press `Ctrl+Shift+Numlock` to activate it.

[About tty art]
	 /etc/rc.local
