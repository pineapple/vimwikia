# sda1 Durmstrang
# sda4 Ilvermony
# sdb1 Hogsmead
# sdb2 Muzix
# sdb3 Castelobroxo
# sdb4 Hogwarts

#!/bin/bash
# sda1 Durmstrang
mount /dev/sda1 ~/storage/Durmstrang

# sda4 Ilvermony
mount /dev/sda4 ~/storage/Ilvermony

# sdb1 Hogsmead
mount /dev/sdb1 ~/storage/Hogsmead

# sdb2 Muzix
mount /dev/sdb2 ~/storage/Muzix

# sdb3 Castelobroxo
mount /dev/sdb3 ~/storage/castelobruxo

# sdb4 Hogwarts
mount /dev/sdb4 ~/storage/Hogswart
