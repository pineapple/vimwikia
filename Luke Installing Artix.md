mkfs.fat -F32 /dev/sdc1

basestrap /mnt base base-devel runit elogind-runit linux  linux-firmware vim

fstabgen /mnt

fstabgen -U /mnt >> /mnt/etc/fstab

artools-chroot /mnt
 bash

 vim /etc/pacman.d/mirrorlist

 ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

 hwclock --systohc

 vim /etc/locale.gen

 vim /etc/local.conf
  LANG=en_US.UTF-8

pacman -S networkmanager networkmanager-runit

ln -s /etc/runit/sv/NetworkManager /run/runit/service

ln -s /etc/runit/sv/NetworkManager /run/runit/runsvdir/current

vim /etc/hostname
  desktop

vim /etc/hosts
   127.0.0.1    localhost
   ::1          localhost
   127.0.0.1    desktop.localdomain desktop

pacman -S grub os-prober efibootmgr

grub-install --target=i386-pc /dev/sdc

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB

grub-mkconfig -o /boot/grub/grub.cfg

passwd

pacman -S wm
