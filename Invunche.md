
Invunche

The Invunche is a creature with origins in mythology, legend and folklore of South America. In particular, accounts of its existence first arise in the mythology, legend and folklore of Chile, among the Chilote people.

In the Chilote folklore and Chilote mythology of the Chiloé Island in southern Chile, the imbunche or invunche (mapudungun ifünche: "deformed person", also "short person") is a legendary monster that protects the entrance to a warlock's cave.

Description

The invunche is a deformed human with its head twisted backwards, along with having twisted arms, fingers, nose, mouth and ears. The creature walks on one foot or on three feet (actually one leg and two hands) because one of its legs is attached to the back of its neck. The invunche can't talk, and communicates only by guttural, rough and unpleasant sounds.

Legend

According to legend, the invunche is a first-born son less than nine days old that was kidnapped by, or sold by his parents to, a Brujo Chilote (a type of sorcerer or warlock of Chiloé). If the baby had been christened, the warlock debaptizes him.

The Brujo chilote transforms the child into a deformed hairy monster by breaking his right leg and twisting it over his back. When the boy is three months old his tongue is forked and the warlock applies a magic cream over the boy's back to cause thick hairs. During its first months the invunche is fed on black cat's milk and goat flesh, and then with human flesh from cemeteries.

Besides guarding the entrance to the warlock's cave, the invunche is used by warlocks as an instrument for revenge or curses. And, because it has acquired magical knowledge over its lifetime spent guarding the cave, even if the invunche is not initiated on wizardry, it sometimes acts as the warlock's adviser.

The invunche leaves the cave only in certain circumstances, such as when the warlock's cave is destroyed or discovered and the warlock moves to another cave, or when the warlocks have need of him, and they carry the invunche while he's thrashing and yelling, scaring the townspeople and announcing misfortune to come. The invunche also comes out when the warlocks take it to the Warlock's Council.

The invunche is fed solely by warlocks and is only allowed to search for its own edibles if food is lacking inside the cave.
