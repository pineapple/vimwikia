*Guided Meditation: Quiet the Mind*

Assume a comfortable position. Shut your eyes as you start relax. Take in a
deep breath, now breathe out, emptying your lungs entirely. And once
again. Take full deep breaths, never strained or rushed. Breathe in strength, breathe out tension. Carry on to relax. Take full deep breaths. Allow your breathing to discover its own natural, unhurried pace. And as ideas enter your mind, allow them go without attachment.

Now listen to the world that exists inside you. Hear the clattering of your
mind, the constant talking to ourselves. See the patterns and habits of the
mind – planning, scheming, calculating, judging, comparing, worrying,
daydreaming. Note when it flies into the past of memories, reliving them
over and over like an addiction to thought. Note when it chases after the
future, attempting to control time by planning and hoping.
But let yourself let all that thinking go, just for a moment. Reassure
yourself, that even if you quit thinking for just a few moments, it’ll be ok.

Give yourself permission to love this moment, without calculating what you
could be doing instead. Encourage yourself that you deserve this time out
from the hectic life you lead, and the frantic thinking you're so accustomed to.

Your breathing is unstrained. Relax your toes and feet. Relax your ankles
and knees. Relax your leg muscles. Your breathing is deep and relaxing.
Relax your fingers and hands. Loosen up your wrists and elbows. Relax
your arms. Your breathing is slow and peaceful. Feel your feet and legs get
heavy and warm. Feel your hands and arms get warm and heavy. Simply
breathe as your thoughts come and go.

Center on your breathing. Let go of tension as your body falls into a stat of peaceful quiet. Unclench your jaw. Relax your face and let go of your
tongue. Your heartbeat and breathing are serene and steady.
Your breathing is relaxed. Void your mind of all thoughts. Your abdomen is soft and warm, your limbs are heavy and warm.

Watch your thoughts come and go like clouds in the sky. Your hands are
warm, your forehead is cool, and your breathing is deep and relaxed. Feel
your breath sink lower and lower into your lungs. As your shoulders drop,
feel your breath fill the upper part of your chest. You're relaxed and
peaceful.

Your breathing is deep and relaxed. Warm hands, cool forehead. Empty
your lungs totally. Hear the silence grow as your mind hushes. Your are
relaxed, calm, and centered. Your heart rate and breathing are calm and
steady. Warm hands, cool forehead. Your breathing is deep and relaxed.
Enjoy the quiet.

When you know it is time to leave this place, begin to return. Let your
breathing bring you back. Gently take a couple of deep breaths. Pause a
moment. Remember one last time that you can return to this place any time
you wish. It may only be for a moment, but it will remind you of the sense
of peace that's yours.
Open your eyes.

Feel the goodness of the meditation for a few moments.
