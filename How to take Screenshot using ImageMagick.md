import utility comes preinstalled with ImageMagick

**with selection**
`import test.png`

**for specific window**
`import -window "$(xdotool getwindowfocus)" test.png`

**for all monitor**
`import -window root test.png`

**for croping specific monitor or resolution**
`import -window root -crop 1920x1880+0+0 test.png`

**for gravity option**
`import -window root -gravity NorthEast -crop 1920x1080+1920+8 test.png`

**for more than one screenshot**
`import -snaps 3 test.png`

**for with delay**
`sleep 3 && import test.png`

# adding effects

**negating colors**
`import -negate test.png`
 - monochrome

**resizing image**
`import -resize 25% test.png`

**rotate image**
`import -rotate 45 test.png`

**setting transparency for colors**
`import -transparent '#33393B' filemanager.png`
