#!/bin/sh

#This is a comment it will just ignored by computer so wont be run or executed

#echo "whats your name?"
#read NAME
#echo $NAME

#read -p "Enter your name: " NAME
#echo "your name is $NAME"

#NAME=edward
#readonly NAME
#NAME=bella
#echo $NAME

#NAME=edward
#unset NAME
#echo $NAME

# $0 = the file name of the script
# $1.....9 = these value correspond with the arguments with which the scripts was invoke
# $# return the number of argument suplied to a script
# $* returns all the arguments that are double quoted
# $@ returns all the arguments that are individaully double qouted
# $? exit status of the last command that you have executed
# $$ will give you the process number of the current shell for the shell script it is also the process id under which it is executed


# echo "File name: $0"
# echo "First parameter: $1"
# echo "second parameter: $2"
# echo "quoted values: $@"
# echo "quoted values: $*"
# echo "No of parameters: $#"

#for TOKEN in $*
#do
#	echo $TOKEN
#done

# Shell Loops
# for loop

#for var in 0 1 2 3 4 5 6 7 8 9
#do
#	echo $var
#done

# while loop

#a=0

#while [ $a -lt 10 ]
#do
#	echo $a
#	a=`expr $a + 1`
#done

# until loop

a=0

until [ ! $a -lt 10 ]
do
	echo $a
	a=`expr $a + 1`
done
