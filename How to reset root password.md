*How to Reset Lost Root account password?*

1. Restart 

2. In grub menu select advanced options

3. Press "e"

4. Scroll down to the last line in "linux /boot/vmlinuz" sentence

5. replace the "ro" with "rw" 

6. And add "init=/bin/bash" at last

7. Press "F10" to reboot

8. In console type these commands
   
   `mount -n -o remount,rw /`

Now reset password 
   
    `passwd root`
    
or

    `passwd username` 

to reboot the computer type 

    `exec /sbin/init`


*METHOD 2: Reset Lost Root Password Using Live CD*

1. Boot from any linux Live CD.

2. Type these commands

- to become root
 
  `sudo su`

- find the location of the hard disk partition

  `fdisk -l`

- in most cases it will be /dev/sda1

- mount it in different folder

  `mkdir /mnt/recover`
  `mount /dev/sda1 /mnt/recover`

- chroot into it

  `chroot /mnt/recover`

- reset the password
   
  `passwd root`

or

  `passwd username`

- exit

- unmount the root partition

  `umount /mnt/recover`

- exit and reboot

*Conclusion*

Changing the root password in Linux is easy, though it requires you to 
venture into the dark realm of the command line. 
Do note that anyone who has access to your computer can use 
this method to reset your superuser or root password. 
One precaution you can take is to encrypt the whole hard disk 
so it can’t be booted or mounted so easily.

