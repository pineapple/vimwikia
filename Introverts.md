*Introverts*

https://youtu.be/T537kDoMzNA

“Our culture made a virtue of living only as extroverts. We discouraged the inner journey, the quest for a center. So we lost our center and have to find it again.” – Anaïs Nin

Extraversion versus introversion is possibly the most recognizable personality trait of the [Big Five](https://www.verywellmind.com/the-big-five-personality-dimensions-2795422). The more of an extravert someone is, the more of a social butterfly they are. Extraverts are chatty, sociable and draw energy from crowds. They tend to be assertive and cheerful in their social interactions.

Introverts, on the other hand, need plenty of alone time, perhaps because their brains process social interaction differently. Introversion is often confused with shyness, but the two aren't the same. Shyness implies a fear of social interactions or an inability to function socially. Introverts can be perfectly charming at parties — they just prefer solo or small-group activities.
