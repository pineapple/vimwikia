1.
Accessibility Options : access.cpl
2.
Add Hardware : hdwwiz.cpl
3.
Add / Remove Programs : appwiz.cpl
4.
Administrative Tools : control admintools
5.
Automatic Updates : wuaucpl.cpl
6.
Wizard file transfer Bluethooth : fsquirt
7.
Calculator : calc
8.
Certificate Manager : certmgr.msc
9.
Character : charmap
10.
Checking disk : chkdsk
11.
Manager of the album (clipboard) : clipbrd
12.
Command Prompt : cmd
13.
Service components (DCOM) : dcomcnfg
14.
Computer Management : compmgmt.msc
15.
DDE active sharing : ddeshare
16.
Device Manager : devmgmt.msc
17.
DirectX Control Panel (if installed) : directx.cpl
18.
DirectX Diagnostic Utility : dxdiag
19.
Disk Cleanup : cleanmgr
20.
System Information : dxdiag
21.
Disk Defragmenter : dfrg.msc
22.
Disk Management : diskmgmt.msc
23.
Partition manager : diskpart
24.
Display Properties : control desktop
25.
Properties of the display (2) : desk.cpl
26.
Properties display (tab "appearance") : control color
27.
Dr. Watson : drwtsn32
28.
Manager vérirficateur drivers : check
29.
Event Viewer : Eventvwr.msc
30.
Verification of signatures of files : sigverif
31.
Findfast (if present) : findfast.cpl
32.
Folder Options : control folders
33.
Fonts (fonts) : control fonts
34.
Fonts folder windows : fonts
35.
Free Cell : freecell
36.
Game Controllers : Joy.cpl
37.
Group Policy (XP Pro) : gpedit.msc
38.
Hearts (card game) : mshearts
39.
IExpress (file generator. Cab) : IExpress
40.
Indexing Service (if not disabled) : ciadv.msc
41.
Internet Properties : inetcpl.cpl
42.
IPConfig (display configuration) : ipconfig / all
43.
IPConfig (displays the contents of the DNS cache) : ipconfig / displaydns
44.
IPConfig (erases the contents of the DNS cache) : ipconfig / flushdns
45.
IPConfig (IP configuration cancels maps) : ipconfig / release
46.
IPConfig (renew IP configuration maps) : ipconfig / renew
47.
Java Control Panel (if present) : jpicpl32.cpl
48.
Java Control Panel (if present) : javaws
49.
Keyboard Properties : control keyboard
50.
Local Security Settings : secpol.msc
51.
Local Users and Groups : lusrmgr.msc
52.
Logout : logoff
53.
Microsoft Chat : winchat
54.
Minesweeper (game) : winmine
55.
Properties of the mouse : control mouse
56.
Properties of the mouse (2) : main.cpl
57.
Network Connections : control NetConnect
58.
Network Connections (2) : ncpa.cpl
59.
Network configuration wizard : netsetup.cpl
60.
Notepad : notepad
61.
NView Desktop Manager (if installed) : nvtuicpl.cpl
62.
Manager links : packager
63.
Data Source Administrator ODBC : odbccp32.cpl
64.
Screen Keyboard : OSK
65.
AC3 Filter (if installed) : ac3filter.cpl
66.
Password manager (if present) : Password.cpl
67.
Monitor performance : perfmon.msc
68.
Monitor performance (2) : perfmon
69.
Dialing Properties (phone) : telephon.cpl
70.
Power Options : powercfg.cpl
71.
Printers and Faxes : control printers
72.
Private Character Editor : eudcedit
73.
Quicktime (if installed) : QuickTime.cpl
74.
Regional and Language Options : intl.cpl
75.
Editor of the registry : regedit
76.
Remote desktop connection : mstsc
77.
Removable Storage : ntmsmgr.msc
78.
requests the operator to removable storage : ntmsoprq.msc
79.
RSoP (traduction. ..) (XP Pro) : rsop.msc
80.
Scanners and Cameras : sticpl.cpl
81.
Scheduled Tasks : control schedtasks
82.
Security Center : wscui.cpl
83.
Console management services : services.msc
84.
shared folders : fsmgmt.msc
85.
Turn off windows : shutdown
86.
Sounds and Audio Devices : mmsys.cpl
87.
Spider (card game) : spider
88.
Client Network Utility SQL server : cliconfg
89.
System Configuration Editor : sysedit
90.
System Configuration Utility : msconfig
91.
System File Checker (SFC =) (Scan Now) : sfc / scannow
92.
SFC (Scan next startup) : sfc / scanonce
93.
SFC (Scan each démarraget) : sfc / scanboot
94.
SFC (back to default settings) : sfc / revert
95.
SFC (purge cache files) : sfc / purgecache
96.
SFC (define size CAHC x) : sfc / cachesize = x
97.
System Properties : sysdm.cpl
98.
Task Manager : taskmgr
99.
Telnet client : telnet
100.
User Accounts : nusrmgr.cpl
101.
Utility Manager (Magnifier, etc) : utilman
102.
Windows firewall (XP SP2) : firewall.cpl
103.
Microsoft Magnifier : magnify
104.
Windows Management Infrastructure : wmimgmt.msc
105.
Protection of the accounts database : syskey
106.
Windows update : wupdmgr
107.
Introducing Windows XP (if not erased) : tourstart
108.
Wordpad : write
109.
Date and Time Properties : timedate.cpl
