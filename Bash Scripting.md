# THESE NOTES TAKEN FROM THE YOUTUBE VIDEO
# "SHELL SCRIPTING CRASH COURSE - BEGINNER LEVEL"
# https://youtu.be/v-F3YLd6oMw
# Uncomment or copy any portion you want to run and save file and chmod it to executable
# chmod a+x bash\ scripting\ basic
# ./bash\ scripting\ basic
#-------------------------------------------------------------
# AUTHOR: VOID00R
#---------------------------------------------------------------------
#
#!/bin/bash
#---------------------------------------------
#VARIABLES
#NAME="jack"
#---------------------------------------------
#COMMAND
#echo "Hello $NAME"
#--------------------------------------------
#USER INPUT
#read -p "Enter your name: " NAME
#echo "your name is $NAME"
#-----------------------------------------
#SIMPLE IF STATEMENT
#if [ "$NAME" == "sam" ]
#then
#	echo "your name is sam"
#elif [ "$NAME" == "jack" ]
#then
#	echo "your name is jack"
#else
#	echo "your name is not sam"
#fi
#-----------------------------------------
#COMPARISON
#NUM1=3
#NUM2=5
#if [ "$NUM1" -gt "$NUM2" ]
#then
#	echo "$NUM1 is greater than $NUM2"
#else
#	echo "$NUM1 is less than $NUM2"
#fi
#--------------------------------------------
# FILE CONDITIONS
#FILE="test.txt"
#if [ -e "$FILE" ]
#then
#	echo "$FILE exists"
#else
#	echo "$FILE does NOT exists"
#fi
#-------------------------------------------------
# CASE STATEMENTS
#read -p "Are you 21 or over? Y/N " ANS
#case "$ANS" in
#	[yY] | [yY][eE][sS])
#		echo "you can have a beer :)"
#	;;
#        [nN] | [nN][oO])
#		echo "Sorry, no drinking"
#	;;
#        *)
#		echo "Say either yes/no [y/n]"
#	;;
#esac
#-------------------------------------------------
# SIMPLE FOR LOOP
#NAMES="Brad Kevin Alice Mark"
#for NAME in $NAMES
#do
#	echo "Hello $NAME"
#done
#-------------------------------------------------------
# FOR LOOP TO RENAME FILES
#FILES=$(ls *.txt)
#NEW="new"
#for FILE in $FILES
#do
#	echo "Renaming $FILE to new-$FILE"
#	mv $FILE $NEW-$FILE
#done
#--------------------------------------------------------
# WHILE LOOP - READ THROUGH A FILE LINE BY LINE
#LINE=1
#while read -r CURRENT_LINE
#do
#	echo "$LINE: $CURRENT_LINE"
#	((LINE++))
#done < "./new-1.txt"
#--------------------------------------------------
# FUNCTIONS
#function sayHello() {
#echo "Hello World"
#}
#
##sayHello
#--------------------------------------
# FUNCTIONS WITH PARAMS
#function greet() {
#	echo "Hello, I am $1 and I am $2"
#}
#
#greet "Brad" "36"
#--------------------------------------------
# CREATE FOLDER AND WRITE TO A FILE
#mkdir hello
#touch "hello/world.txt"
#echo "Hello World" >> "hello/world.txt"
#echo "Created hello/world.txt"
#---------------------------------------
#-------------# THE END #----------------#

#!/bin/sh

#This is a comment it will just ignored by computer so wont be run or executed

#echo "whats your name?"
#read NAME
#echo $NAME

#read -p "Enter your name: " NAME
#echo "your name is $NAME"

#NAME=edward
#readonly NAME
#NAME=bella
#echo $NAME

#NAME=edward
#unset NAME
#echo $NAME

# $0 = the file name of the script
# $1.....9 = these value correspond with the arguments with which the scripts was invoke
# $# return the number of argument suplied to a script
# $* returns all the arguments that are double quoted
# $@ returns all the arguments that are individaully double qouted
# $? exit status of the last command that you have executed
# $$ will give you the process number of the current shell for the shell script it is also the process id under which it is executed


# echo "File name: $0"
# echo "First parameter: $1"
# echo "second parameter: $2"
# echo "quoted values: $@"
# echo "quoted values: $*"
# echo "No of parameters: $#"

#for TOKEN in $*
#do
#	echo $TOKEN
#done

# Shell Loops
# for loop

#for var in 0 1 2 3 4 5 6 7 8 9
#do
#	echo $var
#done

# while loop

#a=0

#while [ $a -lt 10 ]
#do
#	echo $a
#	a=`expr $a + 1`
#done

# until loop

#a=0

#until [ ! $a -lt 10 ]
#do
#	echo $a
#	a=`expr $a + 1`
#done

# loop inside loop

#a=0

#while [ "$a" -lt 10 ]
#do
#	b="$a"
#	while [ "$b" -ge 0 ]
#	do
#		echo -n "$b"
#		b=`expr $b - 1`
#	done
#	echo
#	a=`expr $a + 1`
#done

# infinite loop

#a=10

#until [ $a -gt 0]
#do
#	echo $a
#	a=`expr $a + 1`
#done

&&		executes if first succeded

||		executes if first one failed

;		execute afterwards just like another line even if first failed

&		executes another command simultaneously with first

-z 		means if its equal to nothing

```
#!/bin/sh

if [ "$EDITOR" = "" ]; then
	EDITOR=nano

echo $EDITOR
```

**vs**

```
[ -z "$EDITOR" ] && EDITOR=nano && echo $EDITOR || echo "EDITOR is $EDITOR"
```

**exit codes**

$?	check if cmd successful or not

0 	if command successful
1 	if cmd unsuccessful


**Functions**

```
function unit_test () {
	echo "running unit test"
	return 0
}

unit_test
```
