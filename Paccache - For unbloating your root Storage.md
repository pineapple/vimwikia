*Paccache - Cleanup your pacman cache*

Is your root partition is full so dont worry paccache is here.

When you install a package with Pacman, it stores all downloaded packages
in `/var/caches/pacman/pkg` folder. But pacman didnt remove old and uninstalled
packages automatically by default so after a while the caches size will take
upto the substantial amount of your HDD or SDD.

Fortunately with pacman's built in funtion you can easily clear your caches
with this command `sudo pacman -Sc` but the the problem with this method is
it also clear the caches of your installed packages which are needed when
package update doesnt work and you have to downgrade until the issue is fixed.
Thats not a big disaster because you can easily downgrade a package using Arch repo.

Source: https://endeavouros.com/docs/pacman/paccache/
