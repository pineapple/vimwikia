Book:1 Say What you mean
If I use Nonviolent Communication to liberate people to be less depressed,
to get along better with their family, but do not teach them at the same
time to use their energy to rapidly transform systems in the world, then I
am part of the problem. I am essentially calming people down, making them happier to live in the systems as they are, so I am using NVC as a
narcotic.

    • You first experience your Jing transforming into chi
    • You next experience your chi transforming into shen
    •  You then experience your shen transforming into emptiness,
    • You must then return emptiness to the Tao [for complete realization].

What’s the spiritual process for regaining the natural inherent purity of mental emptiness that enables the return to the Tao? Just let go of everything you’re mentally attaching to, which is the practice of meditation! In other words, just watch the thoughts that arise in your mind without attaching to them or trying to impose comprehension upon them. Eventually, thoughts will die away to reveal a mental state of emptiness, peace, purity and calmness. When any state of mental impurity arises in this empty clarity, don’t give any thought to it and then you’ll spontaneously become free of non-purity. If anybody cultivates in this manner, they’ll naturally climb the ranks of spiritual achievement.


* Anavopaya

- anu individual soul
- concentration on { uccara, karana, dhyana, sthana prakalpana }
- uccara - chakrodaya : breathing deeply and find point between inhale and exhale
- karana - conentrating through sense organ or respected sense. To get one pointedness.
- dhyana - contemplation on particular form or formless idea
- sthana prakalpana - focusing on place/to see vastness of universe in one breath.
  - higher form - when each aspect of reality is discovered in the span of breath
  - lower form - focusing on particular center of body. bhrumadhya, kantha and hridaya

* saktopaya

- functions by energy
- also known as jnanopaya
- centering awareness between any two actions

* sambhavopaya

- functions in matrikachakras, pratyahara, pratibimbavada
- not having the thought and maintaining that state of thoughtlessness

* anupaya

 - mindful action
