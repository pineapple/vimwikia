
   git --version
   git config --list
   git config --global user.name "void soul"
   git config --global user.email "voidsoul@tuta.io"
   git config --list

   # TO INITIALIZE REPOSITORY FROM EXISTING CODE
   `git init`

   -- # TO STOP TRACKING GIT JUST REMOVE .git DIRECTORY

   # LIST STATUS
   `git status`

   -- # TO IGNORE SOME FILES ADD .gitignore file
     wildcarts in .gitignore file
        .DS_STORE
	.project
	*.pyc

    # TO ADD FILES IN STAGING AREA
    `git add .gitignore`
     -- # FOR EVERTHING TO ADD IN STAGING AREA
    	`git add -A`

    # TO REMOVE FILES FROM STAGING AREA
    `git reset calc.py`

    # COMMIT CHANGES
    ` git commit -m "Initial Commit" `

    # SEE LOG
    `git log`

    # CLONING A REMOTE REPO
    git clone <url> <where to clone>

    # TO LIST INFORMATION
    `git remote -v`
    `git branch -v`

    # PUSHING CHANGES
     -- # TO SEE DIFFERENCE
     `git diff`
     -- # TO PULL ANY OTHER CHANGES DONE TO THAT REPO EITHER REMOTELY OR LOCALLY
    `git pull origin main`

    # PUSH IN MASTER BRANCH
    `git push origin main`

    # CREATE A BRANCH FOR NEW FEATURE
    `git branch calc-divide`
    -- list all branch
    `git branch`
    -- Switch to other branch
    `git checkout calc-divide`

    # PUSHING THE BRANCH TO REMOTE REPOSITORY
    `git push -u origin calc-divide`

    # MERGING A BRANCH TO MASTER
    -- TO PULL ANY NEW CHANGES
    `git pull origin master`

    `git merge calc-divide`

    `git push origin master`

    # TO SEE MERGED BRANCH
    `git branch --merged`

    # TO DELETE UNNECCESSSARY BRANCH
    `git branch -d calc-divide`

    # TO LIST ALL BRANCHES REMOTE AND LOCAL
    `git branch -a`

    # TO DELETE BRANCHES FROM REMOTE REPOSITORY
    `git push origin --delete calc-divide`
