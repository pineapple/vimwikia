
- [[How to merge Audio and Video using ffmpeg]]
	`ffmpeg -i video.mp4 -i audio.wav -c:v copy -c:a aac output.mp4`
- Trim Files
	`ffmepg -i file.mkv -ss 00:27:53 -to 00:36:46 -c copy file-2.mkv`
- Downscale Videos
	ffmpeg -i file.mp4 -vf scale=640x480:flags=lanczos -c:v libx264 -preset slow -crf 21 output_compress_480.mp4


ffmpeg -i aliensound.m4a -ss 00:00:00 -to 00:00:08 -c copy t1.m4a`
