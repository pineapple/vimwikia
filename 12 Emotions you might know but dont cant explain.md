*12 Emotions you Might Feel But Can't Explain*
--------------------------
12 UNKNOWN EMOTIONS ==>
--------------------------

	Humans have the amazing ability to feel emotions That are so profound and complex That we sometimes have a hard time expressing them through our language and other means of communication. It can be frustrating when you feel something you can't quite understand or explain.
	This is why the brilliant writer **John Koenig** penned and put together the compendium of all of the most complicated and indescribable feelings you may not even know you are experiencing With that said listed here are the twelve most common emotions you might feel but can't explain.
----------------------------------------------------------------------------

1. Sonder
----------
Sonder is defined as the sudden realization that you can
never completely know a person. It's when you start to understand
that everyone around you has their own story that you might never
get to know about. You realize that you only get to know as
much about a person as they'll allow you to.

2. Hanker Sore
--------------
This is an emotion most of us wouldn't want to admit to feeling
But Hanker Sore refers to the phenomenon of finding someone
so attractive that it makes you feel angry at them for some reason.
It differs from jealousy in the sense that it's subtler
and more subconscious When you feel Hanker Sore, you don't feel
explicitly hostile or envious towards the person
But deep down you can't help but feel upset at their random stroke of luck
for winning the genetic lottery.

3. Lachesism
------------
Have you ever felt the sudden desire to have something bad happen to you?
For no reason Well that feeling is what we call Lachesism.
It's that deep dark impulse most of us have to be struck by tragedy
and come out at the other side. It's born out of our innate desire
for challenges and growth. Which push us to test the bounds of
our courage and resilience.

4. Ellipsism
------------
Ellipsism, as the feeling that people often misinterpret
as curiosity or inquisitiveness. But really it refers to
a desire to understand the world and to know what will
happen in the future. This is something most scientists,
scholars, artists, and inventors can probably relate to
As many of them have a hard time coming into terms with
the fact that they might never get to see how history
will turn out. How they will be remembered or what legacy
they may leave behind.

5. Mauerbauer Traurigkeit
-------------------------
Mauerbauer Traurigkeit is defined as the sudden feeling of
wanting to push people away and put your defenses back up
It often happens when we let our insecurities tell us,
we're not good enough and feel afraid that if other people
saw us for who we really were they'd reject us.

6. Gnossienne
-------------
Similar to Sonder is Gnossienne Which is the feeling you get
when you realize that you don't actually know someone as well
as you thought you did. It's the realization that even your
closest family and friends have a part of themselves that they
hide from everyone else.
A secret inner world that only they will ever know.

7. Liberosis
------------
Have you ever wished that you could just stop worrying so much and relax?
If so you've already experienced Liberosis.
Liberosis is the feeling you get when you're afraid you're not living
your life to the fullest And it comes from the desire to be more carefree
and easygoing.

8. Monachopsis
--------------
Monachopsis means feeling "out-of-place" for some reason.
It can happen even when you're someplace familiar
or surrounded by your friends and family.
You don't understand it.
But you just can't shake off the feeling that you don't belong here
Or you just don't fit in with everyone else.
That's Monachopsis.

9. Onism
--------
Onism refers to the awareness of how little of the world you
will ever be able to experience.
Those of us who love to travel and go on adventures
may sometimes feel wistful over the realization
that there are still so many beautiful places out there
we might never get to see.
Onism represents those unfulfilled desires and goals
we fear we might never achieve before we die.

10. Nighthawk
-------------
This is what you call that feeling you get when you can't
sleep for some reason. Psychologists believe that Nighthawks are
reoccurring thoughts that only come to you late at night.
Maybe because of some unfinished business you need to resolve
but haven't acknowledged yet.

11. Catoptric Tristesse
-----------------------
Similar to Sonder and Gnossienne Catoptric Tristesse is a specific type
of sadness that comes from the realization that you can never really
know what someone else thinks of you. Wanting the approval and acceptance
of others is something all of us feel burdened by.
Which is probably why we all struggle with Catoptric Tristesse
from time to time as well.

12. Jouska
----------
Finally, Jouska is defined as the feeling of wanting to have a
conversation with someone in your head While it may sound somewhat strange
It's something most people have experienced a number of times in their lives.

#Psychology
------------------
Courtesy: [Psych2GO](https://www.youtube.com/watch?v=bny9YViO15o&feature=youtu.be)
------------------
