**2 Best tricks to Transfering Unifinished IDM downloads to Other PC**
Trick 1:
- Goto this location:

(for windows xp)
`C:/Documents and Settings/username/ApplicationData/IDM/DwnlData`

(for windows vista):
`C:/Users/username/AppData/Roaming/IDM/DwnlData`

- copy unfinished file to your pen drive
- Open the IDM
- Select the Unfinished file
- Goto `Tasks > Export > To IDM export file`
- Save it with any name in your pendrive
- Now Goto another computer IDM
- Goto `Tasks > Import > Import the exported file`
- Start Downloading the imported file
- Pause Downloading when it starts Receiving all the Partitions
- Now copy and replace the content of respective folders
- Now resume the Downloading and it will work for sure.

*Trick  2*

- Open `Options > Save To dialog (Options > Downloads)` for older versions and look what folder is set in Temporary directory field.
- Save this folder somewhere in order to use it later.
- Do the same for all categories download folders in order to save your completed downloads.
- At the same dialog look what folders use for default download directories for each category. save these folders as well if necessary.
- Export `HKEY_CURRENT_USER\Software\DownloadMana..` registry key and also save it.
- On another computer import registry key that you saved.
- Copy IDM temporary folder and all default download directories under the same path that they had on initial computer.
- Run IDM.
