# hanuman ji was human and not monkey explained
source: https://youtu.be/fU1fYScQaFc

# In context of Ramayana Wanar word does not mean monkey


# Parents of Hanuman ji

**Father**: kesari - The king of Sumer Mountain kshetra (area)
**Mother**: anjani - daughter of King Kunjer from wanar linage

**Elder Son**: Hanuman ji (source: walmiki ramayan yudhhakand sarg 28, shlok 10)
**Younger 5 brothers**: shrutiman, ketuman, matiman and ghrutiman (source: from pauranik linageawali sources)

# History of Wanar Linage
 - **originated** from **rishi pulha** his spouse **haribhadra**
 - according to mother side of linage it is also called as **hariwansh** or **harigan**
 - later **sub branches** of this linage spread among whole bharatwarsh
 - in **kishkindha 11 branches** were spread: riksh, singh(sih), wanar, wyaghra, sharabh, dwipi, neel, shalyak, marjar, lohas and mayav
 - wanar word also known as **nar(human)** who lives in **wan(forest)**
 - **walmiki ramayan** uses **wanorkash** word for wanar people (which means residents of forest)
 - similar example known today is **pahadi folks** who lives near the **mountain**
 - and place which based on union of **five rivers** called **punjab**
 - like that **folks who resides near the forest** are known as **wanar** at that time.
 - The citizen of cities that based near the forests areas known as **wanar** and **wanorkash**

# Ramayans references:
  - Ravana used word langul for burning his langul.
  - here langul word used for flag of the kingdom and not tail.
  - when hanuman goes to see sita mata in lanka he went with langul i.e. flag of his kingdom kishkindha which he was serving
  -

# हनुमान जी बन्दर नहीं थें इसका प्रमाण
स्रोत :

पिता : केसरी - सुमेर पर्वत के एक भाग के राजा
माता : अनजनी - वानर वंश राजा कुञ्ज की पुत्री

जेष्ट पुत्र : हनुमान जी  (स्रोत: वाल्मीकि रामायण, युद्धकाण्ड सर्ग 28, श्लोक १०)
कनिष्ट ५ भ्राता : श्रुतिमान, केतुमान, मतिमान और घृतिमान (कुछ पौराणिक वंशावली स्रोतो से)

वानर वंश का इतिहास
 - इस वंश का मूल उद्भव ऋषि पुलहा और उनकी पत्नी हरिभद्रा से
 - माता के वंश के आधार पर से इसे हरवंश और हरिगण भी कहते थे
 - बादमे इस वंश के उपवंशीय शाखाये भारत वर्ष में फैली
 - किष्किंधा में ११ शाखाये थी जो की इस प्रकार से है: ऋक्ष, सिंह, वानर, व्याघ्र, शरभ, द्विपी, नील, शल्यक, मार्जर, लोहास और मायाव
 - वानर शब्द का एक यह भी अर्थ मिलता है की नर जो वन में रहते थे
 - और आश्रमव्यवस्था में तीसरे आश्रम वानप्रस्थ आश्रम पहुंचने वाले को वन में निवास करना होता है उन्हें वानप्रस्थी वा वनस्थ भी कहते है.
 - वाल्मीकि रामायण में वनो में रहने वालो के लिए वनोत्कर्ष शब्द का प्रयोग किया है
 - जैसे आज पहाड़ क्षेत्र में रहने वालो को पहाड़ी कहते है
 - पांच नदियो के मेल पर बसे क्षेत्र को पंजाब कहते है
 - वैसे ही वन क्षेत्र के आसपास बसे राज्यो को उस समय वानर और वनोकश कहा जाता था
  -
