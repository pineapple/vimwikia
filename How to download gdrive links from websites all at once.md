How to download gdrive links from websites all at once?

Here our target site is  http://www.phenomenamagazine.co.uk/back-issues/4571429617

1. Open Terminal
Type this command
grep -o "file/d/.*/view" <(curl -s http://www.phenomenamagazine.co.uk/back-issues/4571429617 http://www.phenomenamagazine.co.uk/)

Add awk, to print the middle part:
| awk -F/ '{print $3}'

2. This should print you all the wget commands which are going to be executed:
for id in $(grep -o "file/d/.*/view" <(curl -s http://www.phenomenamagazine.co.uk/back-issues/4571429617) | awk -F/ '{print $3}'); do echo wget -nc --content-disposition "https://drive.google.com/uc?id=$id&export=download"; done

3. Remove Echo Word and execute like this:
for id in $(grep -o "file/d/.*/view" <(curl -s http://www.phenomenamagazine.co.uk/back-issues/4571429617) | awk -F/ '{print $3}'); do wget -nc --content-disposition "https://drive.google.com/uc?id=$id&export=download"; done

Note: -nc flag won't override the existing one in case you rerun the command again
