: ' THESE NOTES TAKEN FROM THE YOUTUBE VIDEO
 "SHELL SCRIPTING CRASH COURSE - BEGINNER LEVEL"
 https://youtu.be/v-F3YLd6oMw
 Uncomment or copy any portion you want to run and save file and chmod it to executable
 chmod a+x bash\ scripting\ basic
 ./bash\ scripting\ basic
-------------------------------------------------------------
 AUTHOR: VOID00R
---------------------------------------------------------------------
'

: ' #!/bin/bash

VARIABLES
NAME="jack"

echo "Hello $NAME"
'
#--------------------------------------------
: ' USER INPUT
read -p "Enter your name: " NAME
echo "your name is $NAME"
'
#-----------------------------------------
: ' SIMPLE IF STATEMENT
if [ "$NAME" == "sam" ]
then
	echo "your name is sam"
elif [ "$NAME" == "jack" ]
then
	echo "your name is jack"
else
	echo "your name is not sam"
fi
'
#-----------------------------------------
: ' COMPARISON
NUM1=3
NUM2=5
if [ "$NUM1" -gt "$NUM2" ]
then
	echo "$NUM1 is greater than $NUM2"
else
	echo "$NUM1 is less than $NUM2"
fi
'
#--------------------------------------------
: ' FILE CONDITIONS
FILE="test.txt"
if [ -e "$FILE" ]
then
	echo "$FILE exists"
else
	echo "$FILE does NOT exists"
fi
'
#-------------------------------------------------
: ' CASE STATEMENTS
read -p "Are you 21 or over? Y/N " ANS
case "$ANS" in
	[yY] | [yY][eE][sS])
		echo "you can have a beer :)"
	;;
        [nN] | [nN][oO])
		echo "Sorry, no drinking"
	;;
        *)
		echo "Say either yes/no [y/n]"
	;;
esac
'
#-------------------------------------------------
: ' SIMPLE FOR LOOP
NAMES="Brad Kevin Alice Mark"
for NAME in $NAMES
do
	echo "Hello $NAME"
done
'
#-------------------------------------------------------
: ' FOR LOOP TO RENAME FILES
FILES=$(ls *.txt)
NEW="new"
for FILE in $FILES
do
	echo "Renaming $FILE to new-$FILE"
	mv $FILE $NEW-$FILE
done
'
#--------------------------------------------------------
: ' WHILE LOOP - READ THROUGH A FILE LINE BY LINE
LINE=1
while read -r CURRENT_LINE
do
	echo "$LINE: $CURRENT_LINE"
	((LINE++))
done < "./new-1.txt"
'
#--------------------------------------------------
: ' FUNCTIONS
function sayHello() {
echo "Hello World"
}

sayHello
'
#--------------------------------------
: ' FUNCTIONS WITH PARAMS
function greet() {
	echo "Hello, I am $1 and I am $2"
}

greet "Brad" "36"
'
#--------------------------------------------
: ' CREATE FOLDER AND WRITE TO A FILE
mkdir hello
touch "hello/world.txt"
echo "Hello World" >> "hello/world.txt"
echo "Created hello/world.txt"
'
#---------------------------------------
#-------------# THE END #----------------#
