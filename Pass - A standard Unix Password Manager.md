   Encrypt your passwords with pass
-----------------------------------------
- Install pass: `sudo pacman -S pass`
- Generate gpg key: `gpg --full-generate-key`
- Remember you key or for see again it
 `gpg --list-secret-keys --keyid-format LONG`
- Initialize your key `pass init "gpg-key"`
- save your password `pass insert testsite.com`
- Enter `pass` to see a list of your saved password
- see your password `pass testsite.com`
- enter your passphrase
--------------------------------------------
- manually decrypting .gpg file
  - change directory to where is your gpg keys located
  - Type `gpg -d testsite.com.gpg`
  - enter your passphrase
--------------------------------------------
Accessing Password from dmenu
- create an keybinding from passmenu
- passmenu copies the password into clipboard for 45 sec
- and also add this line into passmenu file to get notify
`notify-send "Password for $password copied to clipboard for 45sec."
