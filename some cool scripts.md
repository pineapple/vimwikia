a killer program

pid=$(ps -u $USER -o pid,%mem,%cpu,comm | sort -b -k2 -r | sed -n '1!p' | dmenu -i -l 15 | awk '{print $1}')
kill -15 $pid 2>/dev/null
notify-send "you killed process"

create shortlink

file=$(find $HOME -type f | dmenu -i -l 25)
curl -F "file=@$file" 0x0.st | xclip -selection c
notify-send "your file in link"


fzf stuff

`ls -a | fzf -m --layout=reverse-list --border --margin 5% --padding 3% --no-info --prompt=  --pointer=🎃 --marker=🕸️ --header=🍁Fuzzy Surfer --preview='prv {}' --height 70`
