# How to create a live USB from Cli

1. Know your drive
sudo fdisk -l

2. Flash it.
sudo dd bs=4M if=/path/to/endeavouros-x86_64.iso of=/dev/sdX status=progress && sync
-----------------------------------------------------------------------

### How to format USB to Correct way. (Or How to turn it to normal)
- run `df` or `sudo fdisk -l` or `lsblk -l` to list devices
- know your device
unmount it
`sudo umount /dev/sdc1`
- format it
`sudo wipefs --all /dev/sdc`
- now partition it properly with `sudo cfdisk /dev/sdc`
[select msdos for usbs` and create partion as default if not found vfat option in type leave it as linux 83]
- then write filesystem and mount it
`sudo mkfs.vfat -n 'MYUSB' -I /dev/sdc1`
`sudo mkfs.fat -F32 /dev/sdc1`
[The first command will wipe entire data and restore every sector to zero and original so this process is also helpful when formating bootable usbs to normal usbs]

if nothing work to return your pendrive to normal there is one last thing you can do.
- Open Terminal, make sure device is unmounted and run this command
- `sudo dd if=/dev/zero of=/dev/sdc` (No need to add number)
- IT CAN TAKE MUCH TIME so wait for atleast 5 minute
- and then every thing is successful and message shows there is no space left in device then consider its normal
- now you can use above methods to create partition table and format it to vfat.
Thanks to this video It helped me to figure out.
https://youtu.be/V6m5ZPGIbcI

# Usb Errors

**Known Errors**
1. Wrong fst type or badSuper block

```
wrong fs type, bad option, bad superblock on /dev/sdb1,
       missing codepage or helper program, or other error
```
**Solution:**
This will wope out the drive!
- `mkfs.ext4 /dev/sdc1`
- `fdisk -l` If you can see your drive thats good.
- `fsck /dev/sdc1 -y` repairing superblock

2. **Initramfs prompt at startup / Bad Superblock due to power loss**
After a reboot, the boot process drops to the BusyBox shell and I end up at the Initramfs prompt:
`BusyBox v1.18.5 (Ubuntu 1:1.18.5-1ubuntu4) built-in shell (ash) Enter 'help' for a list of built-in commands. (initramfs)`

- Firstly, boot into a live CD or USB
- Find out your partition number by using
  `sudo fdisk -l|grep Linux|grep -Ev 'swap' `

- Then, list all superblocks by using the command:
  `sudo dumpe2fs /dev/sda2 | grep superblock`
  Replace "sda2" to your drive number

- Now, to check and repair a Linux file system using alternate superblock # 32768:
  `sudo fsck -b 32768 /dev/sda2 -y`
  The -y flag is used to skip all the Fix? questions and to answer them all with a yes automatically

- Now try mounting the partition:
`sudo mount /dev/sda2 /mnt`
Now, try to see if device is doing read and write correctly:
`cd /mnt mkdir test ls -l cp file /path/to/safe/location`
If you are able to perform the above commands, you have most probably fixed your error.
- **Now, restart you computer and you should be able to boot normally.**
