# Terminologies
	C -  control key
	M - Meta key, Edit key, Alt key (if not present tap once Esc to simulate it)

	move - "move cursor"
	:: - equivalent to vim key

	c - u <number> <keybind>	- 	prefix argument 	::	similar to motion in vim


# Program Related Commands
	C - x C - c	=	quit emacs
	C - g		=	kill partially hit command
	C - x k		=	stop the tutorial

# Navigations
	C - v		=	page down a screenful
	M - v		=	page up a screenful
	C - l		=	center the page around the cursor

	C - p		=	move upward 		::	k
	C - n		=	move downward		::	j
	C - f		=	move forward 		::	l
	C - b		=	move backward 		::	h

	M - f		=	move wordful forward
	M - b		=	move wordful backward

	C - a		=	beginning of line	::	^
	M - a		=	beginning of sentence
	C - e		=	end of the line		::	$
	M - e		=	end of the sentence

	M - <		=	top of the document	::	gg
	M - >		=	end of the document	::	G
