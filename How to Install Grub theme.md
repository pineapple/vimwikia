1. First Install GRUB
 - `pacman -S grub os-prober efibootmgr` (later packages for efi systems and ability to dual boot)
 - `grub-install --target=i386-pc /dev/sda` for legacy bios
 - `grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB` for UEFI Systems

2. Make config
 `grub-mkconfig -o /boot/grub/grub.cfg`

3. Install theme
 `pacman -S artix-grub-theme`

4. Select theme
 add this line `GRUB_THEME="/usr/share/grub/themes/artix/theme.txt` in `/etc/default/grub`

5. Then update grub with step 2. again.
