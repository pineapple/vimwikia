== My linux Notes ==
# Linux History
  - 1970  - Unix released by Ken Thompson and Dennis Ritchie at AT&T
  - 1977  - BSD released and got into lawsuit by AT&T since it contains unix code
  - 1983  - GNU project started by Richard Stallman to create Free unix-like os and protect it with GPL License
  - 1991  - A personal/hobby kernel project "Linux" started by Finnish Student named Linus Torvalds

Pros of Linux -
	- Free and Open Source
	- Highly Secure
	- More stable and High Performance
	- Less Vulnerable to Malwares and Viruses
	- Frequently Updated and in Development
	- No drivers problem
	- Present in almost types of devices

Cons of Linux -
        - Can Difficult for beginners to use
	- Need more knowledge seeker mentality than consumeristic to work with it.

Philosophy -

  1. Everything is a File
  2. Small, Single-purpose programs
  3. Ability to chain programs together to perform complex tasks
  4. Avoid captive user interface
  5. Configuration data stored in a text file

Components -

  - Bootloader
  - OS Kernel
  - Daemons
  - OS Shell
  - Graphics Server
  - Window Manager
  - Utilities

Architecture

  - Hardware
  - Kernel
  - Shell
  - System Utility

File System Hierarchy
  - / 		Root file System directory
  - /bin	essential binary files directory
  - /boot	files require to boot LinuxOS
  - /dev	device files
  - /etc	local system configuration files
  - /home	users files directory
  - /lib	shared library files
  - /media	directory for external devices mount points
  - /mnt	directory for temporary mount points
  - /opt	directory for optional files like third-party tools
  - /root	home directory for root user
  - /sbin	executable binary files used for system administration
  - /tmp	directory to store temporary files by system applications and users
  - /usr	contains executables, libraries, man files, documentations, etc
  - /var	variable data such as log files, email in boxes, web applications, crons files, etc


# Shell and terminals

# prompts

# getting helps

  - man
  - apropos
  - https://explainshell.com/

# system Information
  - whoami
  - id
  - hostname
  - uname
  - pwd
  - ifconfig
  - ip
  - netstat
  - ss
  - ps
  - who
  - env
  - lsblk
  - lsusb
  - lsof
  - lspci

# User management
  - sudo
  - su
  - useradd
  - userdel
  - usermod
  - addgroup
  - delgroup
  - passwd

# Package Management
  *The features that most package management systems provide are:*

  - Package downloading
  - Dependency resolution
  - A standard binary package format
  - Common installation and configuration locations
  - Additional system-related configuration and functionality
  - Quality control

  - dpkg                    =              A tool to manage .deb packages
  - apt                     =              cli package manager for debian or ubuntu based system
  - aptitude                =              more powerful tool than apt
  - snap                    =              cli tool to manage snap packages
  - gem                     =              front-end to RubyGems, standard package manager for Ruby
  - pip                     =              package manager for python
  - git                     =              git is a versional control system used for storing development files
  - pacman                  =              A powerful package manager for Arch based systems
  - dnf                     =              A powerful package manager for redhat and fedora based systems
  - yay                     =              package manager for Arch user repository

# Service and Process Management

Daemons = server services run in background. has suffix with "d" eg. sshd, systemd
 systemd = init service start at pid 1. It monitors and manage all the other services

PID = all process have assigned pid and can be seen in /proc
systemctl = systemd program to manage services by user

# using openssh

install openssh-client






















Navigation

- cd

Managing Users and Groups

 - Create User `useradd`
 - delete user `userdel`
 - Editing user `usermod`
 - [[Rename user in Linux]]

St

Managing Files and Folders

Mangaging Permissions and Ownership

Package Mangement

Automation
