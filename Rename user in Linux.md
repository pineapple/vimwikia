For renaming user in Linux systems, we will use ‘usermod’ command. Syntax for the command is,

`$ usermod -l new_username old_username`

For example, if we have a user named ‘dan’ & want to rename it to ‘susan’, execute the following command from terminal;

`$ sudo usermod -l susan dan`

This will only change the username & everything else, like group, home directory, UID will remain same.

Note:- You should need to logged out from the account you are trying to rename. You can also kill all the processes running for that user, to do so execute the following command,

`$ sudo pkill -u dan`

`$ sudo pkill -9 -u dan`

Renaming Home directory
For renaming home directory to correspond to the renamed user, we use ‘-d’ option with ‘usermod’ command.,

`$ sudo usermod -d /home/susan -m susan`

Changing UID for the user
To change the UID of the user , execute the following command,

`$ sudo usermod -u 2000 susan`

where ‘2000’ is the new UID for user.

Renaming the group
To rename the group from ‘dan’ to ‘susan‘, we will use ‘groupmod’ command. Use the following command to rename the group,

`$ groupmod -n susan dan`

To use a name that’s already in use but you want to use it anyway, command would be,

`$ groupmod -o -n susan dan`

Once we have made the required changes, we can than check the changes made using the ‘id’ command,

`$ id susan`
