Terminal Banner

========================

##How to make Linux Terminal Banner?
### First Install these packages
- Open Terminal and type
    sudo apt-get install figlet
    sudo apt-get install cowsay
    sudo ln -s /usr/games/cowsay /usr/bin/cowsay

- After Installation done try to run commands first by typing

    figlet your text

_Now you know how it works for more info type man figlet_

     cowsay -f eyes "hi"

type man cowsay for more info
See /usr/share/cowsay/cows for more templates

### For Regular User Terminal
 - Open Terminal
- Switch to root using su root command
- Type
    leafpad .bashrc
 (leafpad is name of text editor it may be differs for you t xed, gedit or nano instead)
- Once .bashrc file opened scroll down to the last line where you found #fi
- Add one more line after #fi line and add these lines:

    figlet anything
    cowsay -f eyes "anthing"

- Now save the files and open new terminal as root to see changes
### For Root Terminal:
- Open Terminal
- Switch to root using su root command
- leafpad /root/.bashrc (nano is name of text editor it may be differs for you try xed, gedit or leafpad instead)
- Once .bashrc file opened scroll down to the last line where you found #fi
- Add one more line by after #fi and add these lines:

     figlet anything
     cowsay -f eyes "anthing"

- Now save the files and open new terminal as root to see changes
- Done, Now every time you open root terminal you will see your banner.
