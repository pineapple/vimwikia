when you launch tmux it creates server
/tmp/tmux-1000 or
/run/user/1000/tmux-1000/default
(dont delete these file)

server > sessions > windows > panes

>_ tmux ls  	- lists sessions
prefix + d	- dettach session

>_ tmux attach	- connect to dittached server

prefix + $	- rename the session
prefix + s	- navigation menu
prefix + c	- creates new window
prefix + ,	- rename the window
prefix + 0-1	- jump to nth window
prefix + n      - jump between windows
prefix + %	- creates horizonatal pane
prefix + "	- creates vertical pane
prefix + arrows	- navigate between panes

>_ tmux new[tab] - creates new [tab options]
>_ tmux list-sessions - duh
>_ tmux kill-[tab] - kills [tab options]

prefix x	- kills the pane
prefix :	- prompt for tmux commands and terminals

~/.config/tmux/tmux.conf
unbind C-b 	# undbinds the default prefix key
set -g prefix C-space 	#sets the prefix key
>_ tmux source tmux.conf
