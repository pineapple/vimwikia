Installing Dwm

#notes

1. Git clone git.suckless.org/dwm

2. make -C /dwm

3. Edit config.mk file
Look for X11NC and change values to /usr/include/X11
and usr/lib/X11

save it and compile it with sudo make clean install

4. goto /usr/share/xsessions/
and create a dwm.desktop file

and write these lines in it

[Desktop Entry]
Encoding=UTF-8
Name=dwm
Comment=dwm window manager
Exec=/usr/local/bin/dwm
Type=Application

save it and exit i3 to and launch dwm
