# Artix/Arch Linux Installation

**1. Base Install**

- keyboard configuration
	`loadkeys us`
- partitions making (It differs from persons choice)
	`fdisk /dev/sda`
	`d 2 n enter +51G w`
	`mkfs.ext4 /dev/sda1`
	`mount /dev/sda1 /mnt`
	`mkdir /mnt/home`
	`mkdir /mnt/boot`

**{Ignore the Above method It just for me heres what i recommend for others}**

	# creating partition table - `cfdisk /dev/sda`
	    ├──	#create partitions according to your need
	    ├── boot (for uefi only)	-	`/dev/sda1` [at least 350mb]
	    ├── root			-	`/dev/sda1` [at least 50Gig]
	    ├── swap			-	`/dev/sda2` [at least 2Gig]
	    └── storage	[optional]	-	`/dev/sda3` [remaining disk part usually 100Gig]
	# Installing filesystem on newly created partitions
	   For Uefi users
	    ├── `mkfs.fat -F 32 /dev/sda1`	<- boot [Only for uefi users]
	    └── `fatlabel /dev/sda1 BOOT`   <- must be labeled as boot
	   For BIOS users
	    ├──	`mkfs.ext4 -L ROOT /dev/sda1`        <- root
	    ├── `mkswap -L SWAP /dev/sda2`           <- swap
	    └── `mkfs.ext4 -L EXTRASTORAGE /dev/sda3`        <- extra for storage, optional
	# Mounting partitions
	    ├── `swapon /dev/sda2`	-> starting swap if created
	    └── `mount /dev/sda1 /mnt`	-> mounting root
	    	└── `mkdir /mnt/home /mnt/boot`  -> creating needed folders

- Installing base system
	`basestrap /mnt base base-devel runit elogind-runit linux linux-firmware vim`
- Generate fstab
	`fstabgen /mnt`
	`fstabgen -U /mnt >> /mnt/etc/fstab`
- chrooting into installed system
	`artools-chroot /mnt`
	`bash`
	`vim /etc/pacman.d/mirrorlist`
- configuring timezone
	`ln -sf /usr/share/zoneinfo/country/city/etc/localtime`
	`hwclock --systohc`
- localization
	`vim /etc/locale.gen`
	`locale-gen`
	`vim /etc/locale.conf`
	`LANG = en_us.UTF-8`
- Configure Network
	`pacman -S NetworkManager NetworkManger-runit`
	`ln -S /etc/runit/sv/NetworkManager /etc/runit/runsvdir/current` #before reboot
	`ln -S /etc/runit/sv/NetworkManager /run/runit/service` # after reboot
	`vim /etc/hostname`
		`desktop`
	`vim /etc/hosts`
		`127.0.0.1	localhost`
		`::1		localhost`
		`127.0.0.1	desktop.localdomain desktop`
- Installing Grub
	`pacman -S grub`
	`grub-install --target=i386-pc /dev/sda` # for bios
     or	`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub` # for uefi
  ├──   `grub-mkconfig -o /boot/grub/grub.cfg`
  │	`pacman -S artix-grub-theme`
  │	`vim /etc/default/grub`
  │	`GRUB_THEME="/usr/share/grub/themes/artix/theme.txt`
  └── # reconfigure grub after installing grub theme
- Add users
	`passwd`
	`useradd -m username`
	`passwd username`
	`usermod -a -G video,audio,input,power,storage,optical,ip,scanner,dbus,adbusers,uucp,vboxusers username`
	`usermod -a -G log mpd network scanner power users video storage ip optical wheel username`
- Installing display server
	`pacman -S xorg`
- Installing Video Drivers
	`pacman -S xf86-video-intel`
- Network Configuration
	#after reboot command for network

**2. Post Base Install**
- Install GUI Desktop Enviroment or Window manager
-
