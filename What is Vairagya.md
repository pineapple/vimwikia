
*What is Vairagya?*
Not only not reacting and responding to pleasureful sensory actions but also not reacting and understanding to non pleasurous and negative thoughts and actions. Vairagya is generally known as detachment, indifference to the sensual knowledge.
“Vairagya” literally means “transparent.” Just as clear flowing water in the river acquires the color of the soil beneath, one who is transparent has no color of his own. Color is a product of the breaking up of light, the basis of our ability to see. Vairagya is an unbroken or “free-of-all-distortion” state that can light the world. It is only in a state of vairagya one can see as It Is.

http://www.swamij.com/yoga-sutras.htm
http://www.ishafoundation.org/soundsofisha/album/vairagya-bonding-with-beyond/
